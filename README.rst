LIBG15
======

This is a library to handle the LCD and extra keys on the Logitech G15 Gaming
Keyboard and similar devices.

=======
Warning
=======
I'm discontinuing this after someone made a fuzz about a feature he decided he want a decade later. 
And as far I am concerned, Arch Linux's AUR administrators find this behavior just fine, so I'm not wasting my efforts on this anymore.
I can still fix issues as I always did and help via mail, but keep in mind Arch Linux is impossible to be supported.

============
Requirements
============

- libusb.

==================
Supported Hardware
==================

- G15 keyboard v1 (blue backlight, 18 'G' keys, LCD)
- G15 keyboard v2 (amber backlight, 6 'G' keys, LCD)
- G110 keyboard
- G510 keyboard v1 (without USB audio)
- G510 keyboard v2 (with USB audio)
- G11 keyboard
- Z10 speakers
- Gamepanel available on some laptops
